import { LitElement, html } from 'lit';

class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String}
        }
    }

    constructor() {
        super();

        this.name = "Rafa";
        this.yearsInCompany = 5;
        this.personInfo = this.setPersonInfo(this.yearsInCompany);

    }

    updated(changedProperties) {
        //console.log(changedProperties);
        changedProperties.forEach(
            (oldValue, propName) => {
                console.log("Propiedad " + propName 
                    + " ha cambiado de valor, anterior era " + oldValue)
            }
        )
        if(changedProperties.has("name")) {
            console.log("Propiedad name cambia de valor, anterior era " 
            + changedProperties.get("name") + " nuevo es " + this.name)
        }
        //console.log(changedProperties);
    }

    render() {
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" value="${this.name}" @change="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" value="${this.yearsInCompany}" @input="${this.updatePersonInfo}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
            </div>
        `
    }

    updateName(e) {
        console.log("> updateName()");
        //console.log(e);

        this.name = e.target.value;

    }

    updatePersonInfo(e) {
        console.log("> updatePersonInfo()");
        //console.log(e);

        this.personInfo = this.setPersonInfo(e.target.value);

    }
    setPersonInfo(yearsInCompany) {
        console.log("> setPersonInfo("+yearsInCompany+")");
        var personInfoReturn = ""
        if (yearsInCompany >= 7) {
            personInfoReturn = "lead";
        }else if (yearsInCompany >= 5) {
            personInfoReturn = "senior";
        }else if (yearsInCompany >= 3) {
            personInfoReturn = "team";
        }else {
            personInfoReturn = "junior";
        }

        return personInfoReturn;

    }
}

customElements.define("ficha-persona", FichaPersona);
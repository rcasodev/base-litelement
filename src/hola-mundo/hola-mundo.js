import { LitElement, html } from 'lit';

class HolaMundo extends LitElement {
    render() {
        return html `
            <div>Hola Mundo!</div>
        `
    }
}

customElements.define("hola-mundo", HolaMundo);
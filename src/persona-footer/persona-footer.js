import { LitElement, html } from 'lit';

class PersonaFooter extends LitElement {

    static get properties() {
        return {            
        };
    }

    constructor() {
        super();
    }

    render() {
        return html `
            <p>@Persona footer</p>
        `
    }
}

customElements.define("persona-footer", PersonaFooter);
import { LitElement, html } from 'lit';

class PersonaMainDm extends LitElement {

    static get properties() {
        return {
            people: { type: Array }


        };
    }

    constructor() {
        super();
        this.people = [{
            name: "Peter Pan",
            yearsInCompany: 2,
            photo: {
                src: "./img/peterPan.jpg",
                alt: "Peter Pan"
            },
            profile: "Algo de profile 1"
        }, {
            name: "Peter Murphy",
            yearsInCompany: 5,
            photo: {
                src: "./img/peterMurphy.jpg",
                alt: "Peter Murphy"
            },
            profile: "Algo de profile 2 Algo de profile 2 Algo de profile 2"
        }, {
            name: "Peter Power",
            yearsInCompany: 15,
            photo: {
                src: "./img/peterPower.jpg",
                alt: "Peter Power"
            },
            profile: "Algo de profile 3 Algo de profile 3 Algo de profile 3 Algo de profile 3 Algo de profile 3"
        }, {
            name: "Peter2 Power",
            yearsInCompany: 15,
            photo: {
                src: "./img/peterPower.jpg",
                alt: "Peter Power"
            },
            profile: "Algo de profile 4 Algo de profile 4 Algo de profile 4 Algo de profile 4 Algo de profile 4 Algo de profile 4 Algo de profile 4"
        }, {
            name: "Peter3 Power",
            yearsInCompany: 15,
            photo: {
                src: "./img/peterPower.jpg",
                alt: "Peter Power"
            },
            profile: "Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5"
        }];
    }

    /*
    render() {
        return html `
            <p>Persona Main Dm</p>
        `
    }
    */

    updated(changedProperties) {
        console.log(">updated() en persona-main-dm");
        console.log(changedProperties);
        if (changedProperties.has("people")) {
            console.log("   ha cambiado el valor de la propiedad people en persona-main-dm");

            this.dispatchEvent(
                new CustomEvent(
                    "created-people", {
                        detail: {
                            people: this.people
                        }
                    }
                )
            )
        }
    }
}

customElements.define("persona-main-dm", PersonaMainDm);
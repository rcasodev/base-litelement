import { LitElement, html } from 'lit';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js';

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: { type: Array },
            showPersonForm: { type: Boolean },
            peopleFilter: { type: Number }
        };
    }

    constructor() {
        super();

        /*this.people = [{
            name: "Peter Pan",
            yearsInCompany: 2,
            photo: {
                src: "./img/peterPan.jpg",
                alt: "Peter Pan"
            },
            profile: "Algo de profile 1"
        }, {
            name: "Peter Murphy",
            yearsInCompany: 5,
            photo: {
                src: "./img/peterMurphy.jpg",
                alt: "Peter Murphy"
            },
            profile: "Algo de profile 2 Algo de profile 2 Algo de profile 2"
        }, {
            name: "Peter Power",
            yearsInCompany: 15,
            photo: {
                src: "./img/peterPower.jpg",
                alt: "Peter Power"
            },
            profile: "Algo de profile 3 Algo de profile 3 Algo de profile 3 Algo de profile 3 Algo de profile 3"
        }, {
            name: "Peter2 Power",
            yearsInCompany: 15,
            photo: {
                src: "./img/peterPower.jpg",
                alt: "Peter Power"
            },
            profile: "Algo de profile 4 Algo de profile 4 Algo de profile 4 Algo de profile 4 Algo de profile 4 Algo de profile 4 Algo de profile 4"
        }, {
            name: "Peter3 Power",
            yearsInCompany: 15,
            photo: {
                src: "./img/peterPower.jpg",
                alt: "Peter Power"
            },
            profile: "Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5 Algo de profile 5"
        }];*/

        this.people = [];
        this.showPersonForm = false;
    }

    render() {
            return html `
           
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">   
                <main class="row row-cols-1 row-cols-sm-4">
                    ${this.people.filter(
                        person => person.yearsInCompany <= this.peopleFilter
                    ).map(              
                        person => html`<persona-ficha-listado
                                        fname="${person.name}"
                                        yearsInCompany="${person.yearsInCompany}"
                                        profile="${person.profile}"
                                        .photo="${person.photo}"
                                        @delete-person="${this.deletePerson}"
                                        @info-person="${this.infoPerson}"></persona-ficha-listado>`
                    )}                
                </main>
            </div>
            <div class="row">
                <persona-form 
                    id="personForm" 
                    class="d-none border rounded border-primary"
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-stored="${this.personFormStored}"></persona-form>
            </div>
            <persona-main-dm @created-people="${this.peopleCreated}"></persona-main-dm>
        `
    }

    updated(changedProperties){
        console.log(">updated() en persona-main");

        if (changedProperties.has("showPersonForm")) {
            console.log("   ha cambiado el valor de la propiedad showPersonForm ["+this.showPersonForm+"] en persona-main");
            if(this.showPersonForm === true) {
                this.showPersonFormData();
            }else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("   ha cambiado el valor de la propiedad people ["+this.people+"] en persona-main");

            this.dispatchEvent(
                new CustomEvent(
                    "people-updated", {
                        detail: {
                            people: this.people
                        }
                    }
                )
            )
        }
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e) {
        console.log(">infoPerson() en persona-main");
        console.log("   Se ha pedido más información de :"+e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }

    showPersonList() {
        console.log(">showPersonList()");
        console.log("   Mostrando el listado de personas...");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFormData() {
        console.log(">showPersonFormData()");
        console.log("   Mostrando el formulario de personas...");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    personFormClose(e) {
        console.log(">personFormClose()");

        this.showPersonForm = false;

    }

    personFormStored(e) {
        console.log(">personFormStored()");
        //console.log(e.detail.person);
        console.log(e.detail);
        //console.log(e.detail.editingPerson);

        if(e.detail.editingPerson === true) {
            console.log("   se va a ACTUALIZAR la persona: " + e.detail.person.name);

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person);
           /*let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );
            
            if(indexOfPerson >= 0) {
                console.log("   persona encontrada!");

                this.people[indexOfPerson] = e.detail.person;

            } */ 
        }else {
            console.log("   se va a ALMACENAR la persona: " + e.detail.person.name);
            //this.people.push(e.detail.person);
            //JS Spread syntax
            this.people = [...this.people, e.detail.person];
        }              

        console.log("<personFormStored()");

        this.showPersonForm = false;

    }

    peopleCreated(e) {
        console.log(">peopleCreated() en persona-app");
        this.people = e.detail.people;
    }
}

customElements.define("persona-main", PersonaMain);
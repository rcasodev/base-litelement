import { LitElement, html } from 'lit';

class PersonaStats extends LitElement {

    static get properties() {
        return {
            people: { type: Array }
        };
    }

    constructor() {
        super();
        this.people = [];
    }

    updated(changedProperties) {
        console.log(">updated() en persona-stats");
        console.log(changedProperties);
        if (changedProperties.has("people")) {
            console.log("   ha cambiado el valor de la propiedad people en persona-stats");

            let peopleStats = this.gatherPeopleArrayInfo(this.people);

            this.dispatchEvent(
                new CustomEvent(
                    "updated-people-stats", {
                        detail: {
                            peopleStats: peopleStats
                        }
                    }
                )
            )
        }
    }

    gatherPeopleArrayInfo(people) {
        console.log(">gatherPeopleArrayInfo()");

        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        let yearsInCompanyMax = 0;
        people.forEach(
            person => {
                if (person.yearsInCompany > yearsInCompanyMax) {
                    yearsInCompanyMax = person.yearsInCompany;
                }
            }
        )

        console.log("   " + yearsInCompanyMax);
        peopleStats.yearsInCompanyMax = yearsInCompanyMax;

        console.log(peopleStats);

        return peopleStats;
    }
}

customElements.define("persona-stats", PersonaStats);
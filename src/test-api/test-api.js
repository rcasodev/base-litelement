import { LitElement, html } from 'lit';

class TestApi extends LitElement {

    static get properties() {
        return {
            movies: { type: Array }
        };
    }

    constructor() {
        super();
        this.movies = [];
        this.getMoviesData();
    }

    render() {
            return html `
            ${this.movies.map(
                movie => html `<div>La pelicula ${movie.title} fue dirigida por ${movie.director}</div>
                `
            )}
        `
    }

    getMoviesData() {
        console.log(">getMoviesData()");
        console.log("   Obteniendo datos de las películas...");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);

                this.movies = APIResponse.results;
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films");
        xhr.send(); //Aquí dentro van los body

        console.log("<getMoviesData()")
    }
}

customElements.define("test-api", TestApi);